class InvoicesController < ApplicationController

  def index
    @invoices = Invoice.all.order(created_at: :desc).paginate(:page => params[:page], :per_page => 25)
  end

  def new
    @invoice = Invoice.new
  end

  def create
    @invoice = Invoice.new(invoice_params)
    if @invoice.save
      redirect_to invoices_path
    else
      render new_invoice_path
    end
  end

  def show
    @invoice = Invoice.find(params[:id])
  end

  def pending
    @invoices = Invoice.all.order(created_at: :desc).paginate(:page => params[:page], :per_page => 25)
  end

  def collected
    @invoices = Invoice.all.order(created_at: :desc).paginate(:page => params[:page], :per_page => 25)
  end

  private
  def invoice_params
    params.require(:invoice).permit(:invoice_date, :narration, :amount, :customer_name, :reference, :brand_manager)
  end
end
