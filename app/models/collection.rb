class Collection < ApplicationRecord
  belongs_to :invoice, foreign_key: :reference, primary_key: :reference, inverse_of: :collections

  validates :reference, :amount, :collection_date, presence: true
end
