class CollectionsController < ApplicationController
  before_action :set_invoice

  def new
    @collection = @invoice.collections.new
  end

  def create
    @collection = @invoice.collections.create(collection_params)
    redirect_to invoice_path(@invoice)
  end



  private
  def collection_params
    params.require(:collection).permit(:reference, :amount, :collection_date)
  end

  def set_invoice
    @invoice = Invoice.find_by(id: params[:invoice_id])
  end
end



