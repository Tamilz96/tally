require 'test_helper'

class InvoicesControllerTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  test 'index' do
    get invoices_path
    assert_response :success
  end

  test 'newInvoicePath' do
    get new_invoice_path
    assert_response :success
  end

  test 'create' do
    assert_difference 'Invoice.count', 1 do
      post invoices_path, params: {invoice: invoice_params}
    end
  end

  private

  def invoice_params
    {
        reference: 'abced',
        narration: 'hello',
        invoice_date: Date.parse('30/12/2019'),
        customer_name: 'raj',
        brand_manager: 'suresh',
        amount: 1000
    }
  end
end
