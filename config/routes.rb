Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root :to => 'invoices#index'

  get 'pendingbills', to: 'invoices#pending'
  get 'collectedbills', to: 'invoices#collected'
  resources :invoices, only: [:index, :new, :create, :show] do
    resources :collections, only: [:new, :create]
  end
end
