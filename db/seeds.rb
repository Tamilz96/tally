# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
invoices = JSON.parse(File.read(File.join(Rails.root, 'config','initializers','invoices.json')))
collections = JSON.parse(File.read(File.join(Rails.root, 'config','initializers','collections.json')))
if invoices.present?
  invoices.each do |invoice|
    Invoice.create(invoice)
  end
end

if collections.present?
  collections.first(1000).each do |collection|
    Collection.create(collection)
  end
end