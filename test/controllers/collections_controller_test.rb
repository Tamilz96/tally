require 'test_helper'

class CollectionsControllerTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  test 'newCollectionPath' do
    get new_invoice_collection_path
    assert_response :success
  end

  test 'createCollection' do
    assert_difference 'Collection.count', 1 do
      post invoice_collections_path(invoice_id: invoice.id), params: {collection: collect_params}
    end
  end

  private

  def invoice
    invoices(:one)
  end

  def collect_params
    {
        reference: 'aagrya',
        collection_date: Date.parse('30/12/2019'),
        amount: 1000
    }
  end
end


