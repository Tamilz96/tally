class CreateCollections < ActiveRecord::Migration[5.2]
  def change
    create_table :collections do |t|
      t.float :amount
      t.string :reference
      t.date :collection_date
      t.timestamps
    end
  end
end

