class Invoice < ApplicationRecord
  has_many :collections, foreign_key: :reference, primary_key: :reference, inverse_of: :invoice

  validates :brand_manager, :customer_name, :amount, :narration, :invoice_date, presence: true


  def balance_due(invoice)
    invoice.amount - invoice.collections.sum(&:amount)
  end
end
